Feature: Verify Truck List has a table and user can select from options 3,10,25,50,100

  @twip
  Scenario Outline: Verify table displays selected option
    Given User is on Truck List Page
    Then Table should have only <option> entries
    And Previous button should be disabled
    And Next button should be enabled if total more than <option>
    And Next button should be disabled on last page and table <option> changing
    Examples:
      | option |
      | 3      |
      | 10     |
      | 25     |
      | 50     |
      | 100    |