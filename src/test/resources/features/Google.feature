@Google @Smoke @Regression
Feature: This feature will allow us to test BDD
  Background: commonly used methods
    Given User is on the "Google" page
#this is just a comment



  Scenario: Searching for the mac book in google (AAA - Assess(Given), Act(When, And), Assert(Then, But))
    When User sends a search request for MacBook
    Then User should be forwarded to another page

    Scenario: Searching for 2 iPhones
      When User sends a search request for 2 "IPhones"
      Then User should be forwarded to another page with "IPhone" in the title

    Scenario: Searching for 3 IPads
      When User sends a search request for 3 "IPad"
      Then User should be forwarded to another page with "IPad" in the title

    Scenario: Searching for 6 iPhones
      When User sends a search request for 6 "IPhones"
      Then User should be forwarded to another page with "IPhone" in the title

    Scenario: Searching for 34 iPhones
      When User sends a search request for 34 "IPhones"
      Then User should be forwarded to another page with "IPhone" in the title