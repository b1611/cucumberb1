@Smoke
Feature: Explicit wait homework practice

  @Explicit @Sanity
  Scenario: Wait for the text to appear after 5 seconds
    Given User navigates to our homework URL
    And User asserts the title
    And Loading please wait element is displayed
    When Loading Complete text appears
    Then User clicks on the last button
    And the text will appear