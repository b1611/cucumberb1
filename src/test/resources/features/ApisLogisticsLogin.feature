
Feature: automation login functionality for Apis Logistics website
Background: navigate to Apis page and validate the title
  Given User is on the "Apis" page
  And the title of the "Apis" page matches

  Scenario: negative username scenario
    When User passes the following usernames and asserts hte password error message is displayed
      | User1 |
      | User2 |
      | User3 |
  @ApisLogistics
  Scenario: negative username and passwrod scenario
    When User passes incorrect username and password then both username error and password error are displayed
      | User1 | Test1 |
      | User2 | Test2 |
      | User3 | Test3 |

  Scenario Outline: positive login scenario
    When User passes "<role>" and "<password>" and the "<URL>" should contain role
    Examples:
      | role        | password       | URL         |
      | manager     | $ManagerApis$  | manager     |
      | dispatch    | $DispatchApis$ | dispatch    |
      | truckdriver | $TruckDriver$  | truckdriver |

