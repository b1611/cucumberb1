#@DemoQA
#Feature: practice with data tables and scenario outlines
#
#
#  Scenario:
#    Given User is on the "DemoQA" page
#    And the title of the "DEMOQA" page matches
#    When User fills up the form with the following data
#      | John              |
#      | Doe               |
#      | JohnDoe@gmail.com |
#      | 123456789         |
#      | 12 July 1995      |
#      | 123 Main st       |
#
#
#  Scenario:
#    Given User is on the "DemoQA" page
#    And the title of the "DEMOQA" page matches
#    When User fills up the form with java faker
#
#  Scenario Outline:
#    Given User is on the "DemoQA" page
#    And the title of the "DEMOQA" page matches
#    When User fills up the form with "<firstName>", "<lastName>", "<email>", "<mobile>" and "<address>"
#    Examples:
#      | firstName | lastName | email                   | mobile     | address               |
#      | Nicholas  | Cage     | NicholasCage@gmail.com  | 8888888888 | 123 York St           |
#      | Michael   | Jordan   | MichaelJordan@gmail.com | 7777777777 | 123 Roosevelt blvd    |
#      | Joe       | Biden    | JoeBiden@gmail.com      | 6666666666 | 1200 Pennsylvania Ave |