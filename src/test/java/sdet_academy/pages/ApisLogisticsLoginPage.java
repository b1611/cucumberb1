package sdet_academy.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sdet_academy.step_definitions.ApisLogisticsLoginStepDefinition;
import sdet_academy.utilities.Driver;

public class ApisLogisticsLoginPage {

    public ApisLogisticsLoginPage(){
        PageFactory.initElements(Driver.getDriver(),this);


    }

    @FindBy(css = "#username-10")
    private WebElement usernameBar;

    @FindBy(xpath = "//div[@class='um-field-error']//span")
    private WebElement passwordErrorMessageElement;

    @FindBy(xpath = "//i[@class='um-icon-ios-close-empty']")
    private WebElement usernameErrorMessageElement;

    @FindBy(css = "#um-submit-btn")
    private WebElement loginButton;

    @FindBy(css = "#user_password-10")
    private WebElement passwordBar;

    public WebElement getUsernameBar() {
        return usernameBar;
    }

    public WebElement getPasswordErrorMessageElement() {
        return passwordErrorMessageElement;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public WebElement getUsernameErrorMessageElement() {
        return usernameErrorMessageElement;
    }

    public WebElement getPasswordBar() {
        return passwordBar;
    }

}
