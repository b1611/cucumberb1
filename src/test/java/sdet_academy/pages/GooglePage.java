package sdet_academy.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sdet_academy.utilities.Driver;

public class GooglePage {

    public GooglePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(name = "q")
    private WebElement searchBar;

    @FindBy(xpath = "//div[@jsname='VlcLAe']//center//input[@class='gNO89b']")
    private WebElement googleSearchButton;

    public WebElement getSearchBar() {
        return searchBar;
    }

    public WebElement getGoogleSearchButton() {
        return googleSearchButton;
    }
}
