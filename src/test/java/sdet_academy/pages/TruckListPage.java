package sdet_academy.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import sdet_academy.utilities.Driver;

import java.util.List;


public class TruckListPage {

    public TruckListPage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(xpath = "//select")
    private WebElement select;

    @FindBy(css = "#tablepress-1_info")
    private WebElement tablePageData;

    @FindBy(css = ".previous")
    private WebElement previous;

    @FindBy(css = ".next")
    private WebElement next;

    @FindBy(xpath = "//tbody//tr")
    private List<WebElement> tableRows;

    public List<WebElement> getTableRows() {
        return tableRows;
    }

    public WebElement getSelect() {
        return select;
    }

    public WebElement getTablePageData() {
        return tablePageData;
    }

    public WebElement getPrevious() {
        return previous;
    }

    public WebElement getNext() {
        return next;
    }

    /**
     *
     * @param index
     * @return element from page data table
     */
    public int getTableData(int index){
        String pageTableDate = this.getTablePageData().getText(); // "Showing 1 to 3 of 91 entries"
        String extractTotal = pageTableDate.split(" ")[index]; // "91"
        // "[Showing,1,to,3,of,91,entries]"
        return Integer.parseInt(extractTotal);
    }

    public static int getTableData(TruckListPage trucks, int index){
        String pageTableDate = trucks.getTablePageData().getText(); // "Showing 1 to 3 of 91 entries"
        String extractTotal = pageTableDate.split(" ")[index]; // "91"
        // "[Showing,1,to,3,of,91,entries]"
        return Integer.parseInt(extractTotal);
    }

    public static int getTableData(String pageText, int index){
        return Integer.parseInt(pageText.split(" ")[index]);
    }

    public static boolean verifyDisabled(WebElement element){
        return element.getAttribute("class").contains("disabled");
    }
}
