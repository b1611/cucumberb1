package sdet_academy.pages;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sdet_academy.utilities.Driver;

import java.util.ArrayList;
import java.util.List;

public class DemoQAPage {
    public DemoQAPage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = "#firstName")
    private WebElement firstnameField;

    @FindBy(css = "#lastName")
    private WebElement lastNameField;

    @FindBy(xpath = "//input[@id='userEmail']")
    private WebElement emailField;

    @FindBy(css = "#dateOfBirthInput")
    private WebElement dateField;

    @FindBy(css = "#currentAddress")
    private WebElement addressField;

    @FindBy(css = "#userNumber")
    private WebElement mobileNumberField;

    public WebElement getFirstnameField() {
        return firstnameField;
    }

    public WebElement getLastNameField() {
        return lastNameField;
    }

    public WebElement getEmailField() {
        return emailField;
    }

    public WebElement getDateField() {
        return dateField;
    }

    public WebElement getAddressField() {
        return addressField;
    }

    public WebElement getMobileNumberField() {
        return mobileNumberField;
    }

    public static List<String> fakeDemoData(){
        Faker faker = new Faker();
        List<String> list = new ArrayList<>();
        list.add(faker.name().firstName());
        list.add(faker.name().lastName());
        list.add(faker.internet().emailAddress());
        list.add(faker.numerify("##########"));
        list.add(faker.chuckNorris().fact());
        return list;
    }
}
