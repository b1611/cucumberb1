package sdet_academy.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sdet_academy.utilities.Driver;

public class ExplicitPracticePage {

    public ExplicitPracticePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//p[contains(text(),'LOADING.. PLEASE WAIT..')]")
    private WebElement loadingPleaseWaitButton;

    @FindBy(xpath = "//p[contains(text(),'LOADING COMPLETE')]")
    private WebElement loadingCompleteButton;

    @FindBy(css = "#click-accordion")
    private WebElement keepClickingButton;

    @FindBy(css = "#hidden-text")
    private WebElement TatianaButton;

    @FindBy(xpath = "//div[contains(text(),'This text has appeared after 5 seconds!')]")
    private WebElement delayedText;

    public WebElement getLoadingPleaseWaitButton() {
        return loadingPleaseWaitButton;
    }

    public WebElement getLoadingCompleteButton() {
        return loadingCompleteButton;
    }

    public WebElement getKeepClickingButton() {
        return keepClickingButton;
    }

    public WebElement getDelayedText() {
        return delayedText;
    }

    public WebElement getTatianaButton() {
        return TatianaButton;
    }
}
