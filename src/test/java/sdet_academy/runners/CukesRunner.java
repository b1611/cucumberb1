package sdet_academy.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {
                "html:target/cucumber-report.html",
                "json:target/cucumber.json"
        },
        features = "src/test/resources/features",
        glue = "sdet_academy/step_definitions",
        dryRun = false,
        tags = "")

public class CukesRunner extends AbstractTestNGCucumberTests {
}
