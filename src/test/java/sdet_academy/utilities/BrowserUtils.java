package sdet_academy.utilities;

import sdet_academy.exceptions.NoSuchPageException;

public class BrowserUtils {

    public static String getFileName(String filePath) {
        return filePath.substring(filePath.lastIndexOf("/") + 1);
    }

    public static void navigateToThePage(String pageName) {
        switch (pageName) {
            case "Google":
                Driver.getDriver().get("https://google.com");
                break;
            case "Etsy":
                Driver.getDriver().get("https://etsy.com");
                break;
            case "Apple":
                Driver.getDriver().get("https://apple.com");
                break;
            case "Apis":
                Driver.getDriver().get("https://qa.apislogisticsinc.com/");
                break;
            case "DemoQA":
                Driver.getDriver().get("https://demoqa.com/automation-practice-form");
                break;
            default:
                throw new NoSuchPageException("No Such Page as " + pageName);
        }
    }

    public static String verifyTitleOnThePage(String pageName) {
        switch (pageName) {
            case "Google":
                return "Google";
            case "Etsy":
                return "Etsy - Shop for handmade, vintage, custom, and unique gifts for everyone";
            case "Apple":
                return "Apple";
            case "Apis":
                return "Apis Logistics Inc";
            case "Trucks":
                return "Trucks – Apis Logistics Inc";
            case "DemoQA":
                return "ToolsQA";
            default:
                throw new NoSuchPageException("No Such Page as " + pageName);
        }
    }
}
