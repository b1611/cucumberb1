package sdet_academy.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationReader {

    // 1. Create property object
    private static Properties properties = new Properties(); // create static object

    static {
        // 2. Load the file into FileInputStream
        try {
            FileInputStream file = new FileInputStream("configuration.properties");
            // 3. load the properties file
            properties.load(file);
        } catch (IOException e) {
            System.out.println("File no found in Configuration Properties");
        }
    }

    public static String getProperty(String keyword){
        return properties.getProperty(keyword);
    }
}
