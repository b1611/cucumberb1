package sdet_academy.exceptions;

public class NoSuchPageException extends RuntimeException{
    public NoSuchPageException(String str){
        super(str);
    }
}
