package sdet_academy.exceptions;

public class NoSuchTitleException extends RuntimeException{
    public NoSuchTitleException(String str){
        super(str);
    }
}
