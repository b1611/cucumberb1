package sdet_academy.step_definitions;

import io.cucumber.java.en.*;
import io.cucumber.java.eo.Se;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import sdet_academy.pages.ApisLogisticsLoginPage;
import sdet_academy.pages.TruckListPage;
import sdet_academy.utilities.BrowserUtils;
import sdet_academy.utilities.Driver;

import java.util.List;

public class TruckListPageStepDefinition {

    ApisLogisticsLoginPage login = new ApisLogisticsLoginPage();
    TruckListPage trucks = new TruckListPage();


    @Given("User is on Truck List Page")
    public void user_is_on_truck_list_page() {
        Driver.getDriver().get("https://qa.apislogisticsinc.com/trucks/");
        login.getUsernameBar().sendKeys("manager");
        login.getPasswordBar().sendKeys("$ManagerApis$");
        login.getLoginButton().click();

        String expectedTitle = BrowserUtils.verifyTitleOnThePage("Trucks");
        Assert.assertEquals(expectedTitle, Driver.getDriver().getTitle());
    }

    @Then("Table should have only {int} entries")
    public void table_should_have_only_entries(int expectedRows) { //3, 10, 25, 50, 100

        /**
         * based on AC we have to verify table has 3 rows by default without selecting it
         */
        List<WebElement> actualRows = trucks.getTableRows();
        Assert.assertEquals(actualRows.size(), 3);

        /**
         * Now we can select option from select dropdown
         */
        Select select = new Select(trucks.getSelect());
        select.selectByValue(String.valueOf(expectedRows));

        /**
         * Verify table is displaying the number of rows selected in Select dropdown
         */
        List<WebElement> actualRowsSelect = trucks.getTableRows();

        // get the total entries (91) from Element - "Showing 1 to 91 of 91 entries"
        String pageTableDate = trucks.getTablePageData().getText();
        int total = TruckListPage.getTableData(pageTableDate, 5);

        // make sure total number of rows is less  option selected in Drop Down
        if (total < expectedRows) {
            Assert.assertEquals(actualRowsSelect.size(), total);
        } else {
            Assert.assertEquals(actualRowsSelect.size(), expectedRows);
        }
    }

    @Then("Previous button should be disabled")
    public void previous_button_should_be_disabled() {

        Assert.assertTrue(TruckListPage.verifyDisabled(trucks.getPrevious()));
    }

    @Then("Next button should be enabled if total more than {int}")
    public void next_button_should_be_enabled_if_total_more_than(int selected) {

        String pageTableDate = trucks.getTablePageData().getText();
        int total = TruckListPage.getTableData(pageTableDate, 5);//91

        if (total > selected) {
            Assert.assertFalse(TruckListPage.verifyDisabled(trucks.getNext()));
        } else {
            Assert.assertTrue(TruckListPage.verifyDisabled(trucks.getNext()));
        }
    }


    @And("Next button should be disabled on last page and table {int} changing")
    public void nextButtonShouldBeDisabledOnLastPageAndTableChanging(int option) {
        WebElement nextButton = trucks.getNext();
        int total = trucks.getTableData(5);
        int expectedFrom = 1;
        int expectedTo = option;
        nextButton.click();

        while (!nextButton.getAttribute("class").contains("disabled")) {
            int from = trucks.getTableData(1);
            int to = trucks.getTableData(3);
            expectedFrom += option;
            expectedTo += option;

            nextButton.click();
            Assert.assertEquals(expectedFrom, from);
            Assert.assertEquals(expectedTo, to);
        }

        Assert.assertTrue(TruckListPage.verifyDisabled(trucks.getNext()));

        if (total > option) {
            Assert.assertEquals(trucks.getTableRows().size(), total % option);
        }
    }
}


