package sdet_academy.step_definitions;

import io.cucumber.java.en.When;
import sdet_academy.pages.DemoQAPage;

import java.util.List;

public class DemoQAStepDefinition {
    DemoQAPage demoQAPage = new DemoQAPage();


    @When("User fills up the form with the following data")
    public void userFillsUpTheFormWithTheFollowingData(List<String> formData) {
        System.out.println(formData.size());
            demoQAPage.getFirstnameField().sendKeys(formData.get(0));
            demoQAPage.getLastNameField().sendKeys(formData.get(1));
            demoQAPage.getEmailField().sendKeys(formData.get(2));
            demoQAPage.getMobileNumberField().sendKeys(formData.get(3));
            demoQAPage.getDateField().clear();
            demoQAPage.getDateField().sendKeys(formData.get(4));
            demoQAPage.getAddressField().sendKeys(formData.get(5));
    }

    @When("User fills up the form with java faker")
    public void userFillsUpTheFormWithJavaFaker() {
        List<String> list = DemoQAPage.fakeDemoData();
        demoQAPage.getFirstnameField().sendKeys(list.get(0));
        demoQAPage.getLastNameField().sendKeys(list.get(1));
        demoQAPage.getEmailField().sendKeys(list.get(2));
        demoQAPage.getMobileNumberField().sendKeys(list.get(3));
        demoQAPage.getAddressField().sendKeys(list.get(4));

    }

    @When("User fills up the form with {string}, {string}, {string}, {string} and {string}")
    public void userFillsUpTheFormWithAnd(String firstName, String lastName, String email, String mobile, String address) {
        demoQAPage.getFirstnameField().sendKeys(firstName);
        demoQAPage.getLastNameField().sendKeys(lastName);
        demoQAPage.getEmailField().sendKeys(email);
        demoQAPage.getMobileNumberField().sendKeys(mobile);
        demoQAPage.getAddressField().sendKeys(address);
    }
}
