package sdet_academy.step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import sdet_academy.pages.GooglePage;
import sdet_academy.utilities.Driver;

public class GoogleStepDefinition {

    GooglePage googlePage = new GooglePage();

    @Given("User navigates to google page")
    public void user_navigates_to_google_page() {
        Driver.getDriver().get("https://google.com");
        Assert.assertEquals(Driver.getDriver().getTitle(), "Google", "Title verification failed");
    }
    @When("User sends a search request for MacBook")
    public void user_sends_a_search_request_for_mac_book() {
        googlePage.getSearchBar().sendKeys("MacBook");
        googlePage.getGoogleSearchButton().click();
    }
    @Then("User should be forwarded to another page")
    public void user_should_be_forwarded_to_another_page() {
        Assert.assertEquals(Driver.getDriver().getTitle(), "MacBook - Google Search", "Title verification failed");
    }

    @When("User sends a search request for {int} {string}")
    public void userSendsASearchRequestFor(int numberOfItems, String nameOfTheItem) {
        googlePage.getSearchBar().sendKeys(numberOfItems + " " + nameOfTheItem);
        googlePage.getGoogleSearchButton().click();
    }

    @Then("User should be forwarded to another page with {string} in the title")
    public void userShouldBeForwardedToAnotherPageWithInTheTitle(String titleDynamicName) {
        Assert.assertTrue(Driver.getDriver().getTitle().contains(titleDynamicName));
    }


}
