package sdet_academy.step_definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.testng.Assert;
import sdet_academy.utilities.Driver;
import static sdet_academy.utilities.BrowserUtils.navigateToThePage;
import static sdet_academy.utilities.BrowserUtils.verifyTitleOnThePage;

public class CommonStepDefinitioin {

    @Given("User is on the {string} page")
    public void user_is_on_the_page(String pageName) {
        navigateToThePage(pageName);
    }

    @And("the title of the {string} page matches")
    public void theTitleOfThePageMatches(String pageName) {
        String expectedTitle = verifyTitleOnThePage(pageName);
        Assert.assertEquals(Driver.getDriver().getTitle(), expectedTitle, "Title doesn't match");
    }
}
