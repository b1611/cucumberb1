package sdet_academy.step_definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import sdet_academy.pages.ExplicitPracticePage;
import sdet_academy.utilities.ConfigurationReader;
import sdet_academy.utilities.Driver;

public class ExplicitPracticeStepDefinition {
    ExplicitPracticePage explicitPracticePage = new ExplicitPracticePage();
    WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 15);

    @Given("User navigates to our homework URL")
    public void user_navigates_to_our_homework_url() {
        Driver.getDriver().get(ConfigurationReader.getProperty("explicitPracticeURL"));
    }
    @Given("User asserts the title")
    public void user_asserts_the_title() {
        Assert.assertEquals(Driver.getDriver().getTitle(), ConfigurationReader.getProperty("explicitPracticeTitle"));
    }

    @And("Loading please wait element is displayed")
    public void loadingPleaseWaitElementIsDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(explicitPracticePage.getLoadingPleaseWaitButton()));
    }

    @When("Loading Complete text appears")
    public void loading_complete_text_appears() {
//        wait.until(ExpectedConditions.visibilityOf(explicitPracticePage.getLoadingCompleteButton()));
//        System.out.println(explicitPracticePage.getTatianaButton().getText());
        wait.until(ExpectedConditions.textToBePresentInElement(explicitPracticePage.getTatianaButton(), "LOADING COMPLETE."));
//        System.out.println(explicitPracticePage.getTatianaButton().getText());
    }

    @Then("User clicks on the last button")
    public void user_clicks_on_the_last_button() {
        explicitPracticePage.getKeepClickingButton().click();
    }
    @Then("the text will appear")
    public void the_text_will_appear() {
        wait.until(ExpectedConditions.visibilityOf(explicitPracticePage.getDelayedText()));
    }


}
