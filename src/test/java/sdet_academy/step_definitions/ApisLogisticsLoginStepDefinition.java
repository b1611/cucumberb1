package sdet_academy.step_definitions;

import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import sdet_academy.pages.ApisLogisticsLoginPage;
import sdet_academy.utilities.Driver;
import java.util.List;
import java.util.Map;

public class ApisLogisticsLoginStepDefinition {
    ApisLogisticsLoginPage apisLogisticsLoginPage = new ApisLogisticsLoginPage();
    WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 15);

    @When("User passes the following usernames and asserts hte password error message is displayed")
    public void userPassesTheFollowingUsernamesAndAssertsHtePasswordErrorMessageIsDisplayed(List<String> usernames) {
        for (String eachUsername : usernames) {
            apisLogisticsLoginPage.getUsernameBar().clear();
            apisLogisticsLoginPage.getUsernameBar().sendKeys(eachUsername);
            apisLogisticsLoginPage.getLoginButton().click();
            Assert.assertTrue(apisLogisticsLoginPage.getPasswordErrorMessageElement().isDisplayed());
        }
    }

    @When("User passes incorrect username and password then both username error and password error are displayed")
    public void userPassesIncorrectUsernameAndPasswordThenBothUsernameErrorAndPasswordErrorAreDisplayed(Map<String, String> credentialsMap) {
        for (Map.Entry<String, String> eachEntrySet : credentialsMap.entrySet()) {
            wait.until(ExpectedConditions.visibilityOf(apisLogisticsLoginPage.getUsernameBar()));
            apisLogisticsLoginPage.getUsernameBar().clear();
            apisLogisticsLoginPage.getUsernameBar().sendKeys(eachEntrySet.getKey());
            wait.until(ExpectedConditions.visibilityOf(apisLogisticsLoginPage.getPasswordBar()));
            apisLogisticsLoginPage.getPasswordBar().clear();
            apisLogisticsLoginPage.getPasswordBar().sendKeys(eachEntrySet.getValue());
            apisLogisticsLoginPage.getLoginButton().click();
            Assert.assertTrue(apisLogisticsLoginPage.getUsernameErrorMessageElement().isDisplayed(), "The Username error message failed");
            Assert.assertTrue(apisLogisticsLoginPage.getPasswordErrorMessageElement().isDisplayed(), "The Password error message failed");
        }
    }

    @When("User passes {string} and {string} and the {string} should contain role")
    public void userPassesAndAndTheShouldContainRole(String role, String password, String URL) {
        apisLogisticsLoginPage.getUsernameBar().sendKeys(role);
        apisLogisticsLoginPage.getPasswordBar().sendKeys(password);
        apisLogisticsLoginPage.getLoginButton().click();
        Assert.assertTrue(Driver.getDriver().getCurrentUrl().contains(URL));
    }
}
